package com.example.mobilevisiontext;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;

public class BarcodeActivity extends AppCompatActivity {

    private TextView barcodeNum;
    private ImageView barcodeImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode);

        init();
    }

    private void init() {
        barcodeNum = findViewById(R.id.barcode_num);
        barcodeImage = findViewById(R.id.barcode_image);

        getIntentData();

    }

    private void getIntentData() {
        Intent intent = getIntent();
        String barcode = intent.getStringExtra("barcodeNum");
        barcodeNum.setText(barcode);
        barcodeImage.setImageBitmap(createBarcode(barcode));
        barcodeImage.invalidate();
    }

    private Bitmap createBarcode(String code) {
        Bitmap bitmap = null;

        MultiFormatWriter gen = new MultiFormatWriter();
        try {
            final int WIDTH = 840;
            final int HEIGHT = 160;
            BitMatrix bytemap = gen.encode(code, BarcodeFormat.CODE_128, WIDTH, HEIGHT);
            bitmap = Bitmap.createBitmap(WIDTH, HEIGHT, Bitmap.Config.ARGB_8888);
            for(int i=0; i < WIDTH; ++i) {
                for (int j = 0; j < HEIGHT; ++j) {
                    bitmap.setPixel(i, j, bytemap.get(i, j) ? Color.BLACK : Color.WHITE);
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }

        return bitmap;
    }
}
