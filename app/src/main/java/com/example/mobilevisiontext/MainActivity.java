package com.example.mobilevisiontext;

import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

import java.io.InputStream;

public class MainActivity extends AppCompatActivity {
    private String LOG_TAG = "testLog";
    private Button testButton;
    private Button mobileVisionRecogBarcode;
    private TextView testText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    private void init() {
        testButton = findViewById(R.id.tesButton);
        testButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, 1);
            }
        });
        mobileVisionRecogBarcode = findViewById(R.id.recog_barcode_mobile_vision);
        mobileVisionRecogBarcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, 2);
            }
        });
        testText = findViewById(R.id.testText);

    }

    private void recognizeBarcode_mobileVisioni(Bitmap selectImage) {
        Log.w("test", "바코드 모바일 비전");
        Bitmap imageBitmap = selectImage;
        BarcodeDetector detector =
                new BarcodeDetector.Builder(getApplicationContext())
                        .setBarcodeFormats(Barcode.ALL_FORMATS )
                        .build();
        if(!detector.isOperational()){
            testText.setText("Could not set up the detector!");
            Log.w("test", "바코드 디텍터 만듬 실패");
            return;
        }

        Frame frame = new Frame.Builder().setBitmap(imageBitmap).build();
        SparseArray<Barcode> barcodes = detector.detect(frame);
        Barcode thisCode = barcodes.valueAt(0);

        testText.setText("value : "  + thisCode.rawValue + ", format : " + BarcodeFormatter.decodeFormat(thisCode.format));
    }

    private void recognize(Bitmap selectImage) {
        Bitmap imageBitmap = selectImage;
        if(imageBitmap != null) {

            TextRecognizer textRecognizer = new TextRecognizer.Builder(this).build();

            if(!textRecognizer.isOperational()) {
                // Note: The first time that an app using a Vision API is installed on a
                // device, GMS will download a native libraries to the device in order to do detection.
                // Usually this completes before the app is run for the first time.  But if that
                // download has not yet completed, then the above call will not detect any text,
                // barcodes, or faces.
                // isOperational() can be used to check if the required native libraries are currently
                // available.  The detectors will automatically become operational once the library
                // downloads complete on device.
                Log.w(LOG_TAG, "Detector dependencies are not yet available.");

                // Check for low storage.  If there is low storage, the native library will not be
                // downloaded, so detection will not become operational.
                IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
                boolean hasLowStorage = registerReceiver(null, lowstorageFilter) != null;

                if (hasLowStorage) {
                    Toast.makeText(this,"Low Storage", Toast.LENGTH_LONG).show();
                    Log.w(LOG_TAG, "Low Storage");
                }
            }


            Frame imageFrame = new Frame.Builder()
                    .setBitmap(imageBitmap)
                    .build();

            SparseArray<TextBlock> textBlocks = textRecognizer.detect(imageFrame);

            for (int i = 0; i < textBlocks.size(); i++) {
                TextBlock textBlock = textBlocks.get(textBlocks.keyAt(i));

                Log.e(LOG_TAG, textBlock.getValue());
                // Do something with value
                String detectedStr = textBlock.getValue().replaceAll(" ", "");
                if(detectedStr.length() == 16 && Utils.isNumeric(detectedStr)) {
                    startBarCodeActivity(detectedStr);
                }
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                try {
                    // 선택한 이미지에서 비트맵 생성
                    Log.w("test", "Request 1");
                    InputStream in = getContentResolver().openInputStream(data.getData());
                    Bitmap img = BitmapFactory.decodeStream(in);
                    in.close();
                    // 이미지 표시
                    recognize(img);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if(requestCode == 2) {
            if (resultCode == RESULT_OK) {
                try {
                    // 선택한 이미지에서 비트맵 생성
                    Log.w("test", "Request 2");
                    InputStream in = getContentResolver().openInputStream(data.getData());
                    Bitmap img = BitmapFactory.decodeStream(in);
                    in.close();
                    // 이미지 표시
                    recognizeBarcode_mobileVisioni(img);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void startBarCodeActivity(String num) {
        Intent intent = new Intent(this.getApplicationContext(), BarcodeActivity.class);
        intent.putExtra("barcodeNum", num);
        startActivity(intent);
    }
}
